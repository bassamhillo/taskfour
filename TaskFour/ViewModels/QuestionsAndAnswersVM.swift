//
//  QuestionsAndAnswersVM.swift
//  TaskFour
//
//  Created by Julia on 7/20/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation

class ExamVM:NSObject {
    var examTimer:ExamTimer?
    var exam:Exam?
    var questionsForPages:[[QuestionAndAnswer]] = []
    var pageCount:Double?
    var numberOfQuestionsPerPage:Int?
    var questionCount:Int?

    init(_ exam:Exam) {
        super.init()
        self.exam = exam
        examTimer = ExamTimer(duration: self.exam!.time)
        
        self.numberOfQuestionsPerPage = exam.numberOfQuestionsPerPage
        self.questionCount = self.exam?.questions.count

        
        self.pageCount = ceil(Double (questionCount!) / Double (numberOfQuestionsPerPage!))
        
        self.fillPagesQuestions()
        
        
    }
    
    func fillPagesQuestions() {
        var pageIndex = 0
        var questionIndex = 0
        var limit = questionIndex + (numberOfQuestionsPerPage!)
        
        while (pageIndex < Int(self.pageCount!)) {
            questionsForPages.append([])
            while questionIndex < limit && questionIndex <  questionCount! {
                questionsForPages[pageIndex].append((self.exam?.questions[questionIndex])!)
                questionIndex += 1
            }
            pageIndex += 1
            limit += numberOfQuestionsPerPage!
        }
    }

}
