//
//  DataManager.swift
//  TaskFour
//
//  Created by Julia on 7/21/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation
import UIKit

class DataManager:NSObject {
    
    private var questionsAndAnswers: [QuestionAndAnswer] = []
    private var exam: Exam = Exam()
    private var elementName:String = ""
    private var question:String = ""
    private var answers:[String] = []
    
    private var parserComplitionHandler: ((Exam) -> Void)?

    func readExam(_ complitionHandler: ((Exam) -> Void)?) {
        self.parserComplitionHandler = complitionHandler
        
        if let path = Bundle.main.url(forResource: "Questions", withExtension: "xml") {
            if let parser = XMLParser(contentsOf: path) {
                parser.delegate = self
                parser.parse()
            }
        }
    }
}

extension DataManager:XMLParserDelegate{
    // MARK: - XML Parser Delegates to get data from XML
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {

        if elementName == "question" {
            if attributeDict["title"] != nil {
                question = attributeDict["title"]!
            }
        } else if elementName == "answers" {
            answers = []
        } else if elementName == "option" {
            if attributeDict["title"] != nil {
                answers.append(attributeDict["title"]!)
            }
        } else if elementName == "settings" {
            if attributeDict["examDuration"] != nil && Int(attributeDict["examDuration"]!) != nil {
                exam.time = Int(attributeDict["examDuration"]!)!
            }
            if attributeDict["numberOfQuestionsPerPage"] != nil && Int(attributeDict["numberOfQuestionsPerPage"]!) != nil {
                exam.numberOfQuestionsPerPage = Int(attributeDict["numberOfQuestionsPerPage"]!)!
            }
            if attributeDict["eachQuestionWeight"] != nil && Int(attributeDict["eachQuestionWeight"]!) != nil {
                exam.eachQuestionWeight = Int(attributeDict["eachQuestionWeight"]!)!
            }
        }
        self.elementName = elementName
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "question" {
            let questionAndAnswer = QuestionAndAnswer()
            questionAndAnswer.question = question
            questionAndAnswer.answers = answers
                
            questionsAndAnswers.append(questionAndAnswer)
            exam.questions = questionsAndAnswers
        }
    }

    func parserDidEndDocument(_ parser: XMLParser) {
        parserComplitionHandler!(exam)
    }
    
}
