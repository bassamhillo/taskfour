//
//  ExamTimer.swift
//  TaskFour
//
//  Created by Julia on 7/21/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation
import UIKit

class ExamTimer:NSObject {
    var timer: Timer?
    var timeInterval = 1.0
    var seconds:Int = 0
    var duration:Int?
    
    var timeTxt:Observable<String?> = Observable("Time starting")
    
    init(duration: Int) {
        self.duration = duration
        self.seconds = self.duration! * 60
    }
    
    func start() {
        timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(dicressTime), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode: .common)
    }
    
    @objc func dicressTime() {
        seconds -= 1

        let hours = seconds / 3600
        let minutes = (seconds % 3600) / 60
        let secounds = (seconds % 3600) % 60

        timeTxt.value = "\(converToTwoDigit(hours)):\(converToTwoDigit(minutes)):\(converToTwoDigit(secounds))"

        if seconds <= 0 {
            timer!.invalidate()
        }
    }
    
    func converToTwoDigit(_ number: Int) -> String {
        if number == 0 {
            return "00"
        } else if (number / 10) >= 1 {
            return "\(number)"
        } else {
            return "0\(number)"
        }
    }
    
}
