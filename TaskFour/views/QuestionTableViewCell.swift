//
//  QuestionTableViewCell.swift
//  TaskFour
//
//  Created by Julia on 7/21/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class QuestionTableViewCell: UITableViewCell {

    @IBOutlet weak var answersStackView: UIStackView!
    @IBOutlet weak var questionLabel: UILabel!
    
    private var radioButtons:[UIButton] = []
    
    var answerArray:[AnswerCell] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setQuestionAndAnswers(questionAndAnswer:QuestionAndAnswer) {
        questionLabel.text = questionAndAnswer.question
        
        for label in answerArray {
            answersStackView.removeArrangedSubview(label)
            label.removeFromSuperview()
        }
        answerArray.removeAll()
        
        for answer in questionAndAnswer.answers {
            if let answerView = Bundle.main.loadNibNamed("AnswerCell", owner: self, options: nil)?.first as? AnswerCell {
                answerView.answerLabel.text = answer
                
                let button = answerView.checkBoxButton
                
                button?.addTarget(self, action: #selector(QuestionTableViewCell.buttonDown(sender:)), for: .touchDown)

                answerArray.append(answerView)
                radioButtons.append(button!)
                radioButtons.append(answerView.checkBoxButton)
                answersStackView.addArrangedSubview(answerView)
            }
        }
    }
    
    @objc func buttonDown(sender:UIButton) {
        for button in radioButtons {
            button.isSelected = false
        }
        
        sender.isSelected = true
    }

}
