//
//  QuestionTableView.swift
//  TaskFour
//
//  Created by Julia on 7/21/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class QuestionTableView: UITableView, UITableViewDataSource, UITableViewDelegate {
    
    var examVM:ExamVM?
    var pageNumber:Int? {
        didSet {
            self.data = self.examVM!.questionsForPages[pageNumber!]
        }
    }
    var data:[QuestionAndAnswer]?
    private var questions:[QuestionAndAnswer] = []
    private var totalQuestionsCount:Int?

    func initalize(examVM:ExamVM, pageNumber:Int) {
        self.examVM = examVM
        self.pageNumber = pageNumber
        self.totalQuestionsCount = examVM.questionCount!
        
        self.delegate = self
        self.dataSource = self
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell", for: indexPath) as! QuestionTableViewCell
        
        cell.setQuestionAndAnswers(questionAndAnswer: self.data![indexPath.row])
                
        return cell
    }

    @objc func loadTable() {
        self.reloadData()
    }
}
