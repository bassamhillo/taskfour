//
//  AnswerCell.swift
//  TaskFour
//
//  Created by Julia on 7/22/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class AnswerCell: UIView {
    
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var answerLabel: UILabel!
    
}
