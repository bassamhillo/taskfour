//
//  ExamViewController.swift
//  TaskFour
//
//  Created by Julia on 7/21/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit

class ExamViewController: UIViewController {
    
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var NumberOfQuestionLabel: UILabel!
    @IBOutlet weak var marksLabel: UILabel!
    
    @IBOutlet weak var questionTableView: QuestionTableView!

    var dataManager = DataManager()
    var examVM:ExamVM?
    var exam: Exam?
    var numberOfQuestionsPerPage:Int = 0
    var eachQuestionMark:Int = 0
    var pageIndex:Int = 0
    var pageCount:Double?

    override func viewDidLoad() {
        super.viewDidLoad()

        dataManager.readExam { (data) in
            self.exam = data
            self.examVM = ExamVM(self.exam!)
            
            // Start the timer
            self.examVM?.examTimer?.start()
                        
            self.numberOfQuestionsPerPage = self.examVM?.numberOfQuestionsPerPage! as! Int
            self.eachQuestionMark = self.examVM?.exam?.eachQuestionWeight as! Int
            
            // Initialize the table
            self.questionTableView.initalize(examVM: self.examVM!, pageNumber:0)
            
            self.pageCount = self.examVM?.pageCount
        }
        
        examVM?.examTimer?.timeTxt.bind {
            self.timerLabel.text = "Timer :(\($0!))"
         }
        
        self.NumberOfQuestionLabel.text = "Number of question per page : \(examVM?.exam?.numberOfQuestionsPerPage ?? 0)"
        
        self.marksLabel.text = "Each question weight : \(self.eachQuestionMark)"
        
        questionTableView.tableFooterView = UIView(frame: .zero)
        
        if pageCount! > 1.0 {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextPage))
        }
    }
}

extension ExamViewController {
    @objc func nextPage() {
        
        // increase the page index
        pageIndex += 1
        
        // hide next button if it's the last page
        if Double(pageIndex) == pageCount! - 1 {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        if pageIndex != 0 && self.navigationItem.leftBarButtonItem == nil{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Previous", style: .plain, target: self, action: #selector(previousPage))
        }
        
        self.questionTableView.pageNumber = pageIndex
        self.questionTableView.reloadData()
    }
    
    @objc func previousPage() {
        pageIndex -= 1

        if pageIndex == 0 {
            self.navigationItem.leftBarButtonItem = nil
        }
        
        if Double(pageIndex) != pageCount! - 1 && self.navigationItem.rightBarButtonItem == nil{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextPage))
        }
        
        self.questionTableView.pageNumber = pageIndex
        self.questionTableView.reloadData()

    }
}
