//
//  Exam.swift
//  TaskFour
//
//  Created by Julia on 7/21/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import Foundation

class Exam{
    var questions:[QuestionAndAnswer] = []
    var time: Int = 0
    var numberOfQuestionsPerPage: Int = 0
    var eachQuestionWeight: Int = 0
}
